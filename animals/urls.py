from django.urls import path
from . import views

urlpatterns = [
    path('api/animals/', views.AnimalsView.as_view()),
    path('api/animals/edit/<int:id>/', views.animal_edit_detail),
    path('api/animals/<int:id>/', views.animal_detail),
    path('api/email/', views.email_request),
    path('api/requests/', views.requests),
    path('api/org_animals_list/', views.org_animals_list),
    path('api/countrylist/', views.countrylist),
    path('api/country/', views.add_country),
    path('api/city/', views.add_city),
    path('api/citylist/<int:id>/', views.citylist),
    path('api/images/', views.ImagesUpload.as_view()),
    path('api/index-view/', views.index_view),
    path('api/remove-images/<int:id>/', views.remove_image),
]