from rest_framework.response import Response
from django.utils.decorators import method_decorator
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import generics
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from .models import Animal, Request, City, Country, Image
from .serializers import *
from django.views.decorators.csrf import csrf_exempt
import json
from django.db.models import Q

class AnimalsView(APIView):
    def get(self, request, *args, **kwargs):
        type = self.request.query_params.get('type', None)
        gender = self.request.query_params.get('gender', None)
        age = self.request.query_params.get('age', None)
        health = self.request.query_params.get('health', None)
        country = self.request.query_params.get('country', None)
        transfer = self.request.query_params.get('transfer', None)
        order = self.request.query_params.get('sort', None)
        animals = Animal.objects.filter(status='active')
        if type:
            animals = animals.filter(species__in=[type])
        if gender:
            animals = animals.filter(gender__in=[gender])
        if age:
            animals = animals.filter(age__in=[age])
        if health:
            animals = animals.filter(health__in=[health])
        if country:
            if transfer == 'true':
                animals = animals.filter(Q(transfer=True) | Q(country=country))
            else:
                animals = animals.filter(country__in=[country])

        animals = animals.order_by(order)
        serializer = AnimalGetSerializer(animals, many=True)
        return Response(serializer.data)
    

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = request.data
        org = request.user
        data['org'] = org.id
        photos = data.pop('photos')
        serializer = AnimalSerializer(data=data, context={'request': photos})
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
 
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ImagesUpload(APIView):
    parser_classes = (MultiPartParser, FileUploadParser)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = ImageSerializer(data=data, context={'request': request}, many=True)
        if serializer.is_valid():
            obj = serializer.save()
            ids = [o.id for o in obj]
            d = Image.objects.filter(id__in=ids) 
            s = ImageGetSerializer(d, many=True)
            return Response(s.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def animal_edit_detail(request, id):
    try:
        animal = Animal.objects.get(pk=id)
    except Animal.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        animal = AnimalSerializer(animal)
        return Response(animal.data)


@api_view(['PUT', 'DELETE', 'GET'])
def animal_detail(request, id):
    try:
        animal = Animal.objects.get(pk=id)
    except Animal.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        animal = AnimalGetSerializer(animal)
        return Response(animal.data)

    elif request.method == 'PUT':
        data = request.data
        photos = data.pop('photos')
        serializer = AnimalSerializer(animal, data=data, context={'request': photos})
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        animal.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def org_animals_list(request):
    if request.method == 'GET':
        data = Animal.objects.filter(org=request.user)

        serializer = AnimalSerializer(data, context={'request': request}, many=True)

        return Response(serializer.data)

@csrf_exempt
def email_request(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    email = body['email']
    text = body['text']
    id = body['id']
    name = body['name']
    user = request.user
    animal = Animal.objects.get(pk=id)
    Request.objects.create(animal=animal, org=animal.org, user=user, email=email, name=name, message=text)
    org_email = animal.org.email
    email_subject = '[adoptpet.eu] New request'
    html_message = render_to_string('animals/request_mail.html', {'name': name, 'text': text, 'email': email, 'animal': animal})
    plain_message = strip_tags(html_message)
    from_email = 'AdoptPet <noreply@adoptpet.eu>'
    to_email = org_email
    send_mail(email_subject, plain_message, from_email, [to_email], html_message=html_message)
    return HttpResponse(status=200)

@api_view(['POST'])
def add_country(request):
    if request.method == 'POST':
        data = request.data
        serializer = CountrySerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            country = Country.objects.all()
            serializer = CountrySerializer(country, many=True)
            return Response(serializer.data, status=status.HTTP_201_CREATED) 
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def add_city(request):
    if request.method == 'POST':
        data = request.data
        serializer = CitySerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            city = City.objects.filter(country=data['country'])
            serializer = CitySerializer(city, many=True)
            return Response(serializer.data, status=status.HTTP_201_CREATED) 
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def citylist(request, id):
    if request.method == 'GET':
        data = City.objects.filter(country=id)
        serializer = CitySerializer(data, context={'request': request}, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        data = request.data
        org = request.user
        data['org'] = org.id
        serializer = AnimalSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
            
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def countrylist(request):
    if request.method == 'GET':
        data = Country.objects.all()
        serializer = CountrySerializer(data, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        data = request.data
        org = request.user
        data['org'] = org.id
        serializer = AnimalSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
            
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def index_view(request):
    if request.method == 'GET':
        data = Animal.objects.filter(status='active').order_by('?')[:3]
        serializer = AnimalGetSerializer(data, many=True)
        total = Animal.objects.filter(status='active').count() - 3
        if total < 0:
            total = 0
        else:
            total = total
        return Response({
            "animal": serializer.data,
            "total": total,
        })

@api_view(['DELETE'])
def remove_image(request, id):
    try:
        image = Image.objects.get(pk=id)
    except Animal.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'DELETE':
        image.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def requests(request):
    if request.method == 'GET':
        user = request.user
        data = Request.objects.filter(Q(org=user) | Q(user=user)).order_by('-created')
        serializer = RequestsGetSerializer(data, many=True)
        return Response({
            "requests": serializer.data,
            "user_id": user.id,
        })