from rest_framework import serializers
from .models import Animal, Country, City, Image, Request
from easy_thumbnails.templatetags.thumbnail import thumbnail_url


class ImageListSerializer(serializers.ListSerializer):
    def create(self, validated_data):
        photos_data = self.context.get('request').FILES.getlist('photos')
        data = [Image.objects.create(photo=data) for data in photos_data]
        return data

class ImageSerializer(serializers.Serializer):
    class Meta:
        list_serializer_class = ImageListSerializer
        model = Image
        fields = ('id', 'photo')

class ThumbnailSerializer(serializers.ImageField):
    def __init__(self, alias, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.read_only = True
        self.alias = alias

    def to_representation(self, value):
        if not value:
            return None

        url = thumbnail_url(value, self.alias)
        request = self.context.get('request', None)
        if request is not None:
            return request.build_absolute_uri(url)
        return url

class ImageGetSerializer(serializers.ModelSerializer):
    photo = ThumbnailSerializer(alias='thumb')
    class Meta:
        model = Image
        fields = '__all__'

class AnimalGetSerializer(serializers.ModelSerializer):
    photos = serializers.SerializerMethodField('get_photos_list')
    country = serializers.SerializerMethodField('get_country_name')
    city = serializers.SerializerMethodField('get_city_name')
    gender = serializers.SerializerMethodField('get_gender')
    health = serializers.SerializerMethodField('get_health')
    age = serializers.SerializerMethodField('get_age')
    class Meta:
        model = Animal
        extra_kwargs = {"photos": {"required": False, "allow_null": True}}
        fields =(
            "id",
            "species",
            "name",
            "description",
            "age",
            "health",
            "gender",
            "city",
            "country",
            "status",
            "org",
            "photos",
            "transfer"
        )
    def get_country_name(self, obj):
        return obj.country.country

    def get_city_name(self, obj):
        return obj.city.city

    def get_gender(self,obj):
        return obj.get_gender_display()
    
    def get_health(self,obj):
        return obj.get_health_display()
    
    def get_age(self,obj):
        return obj.get_age_display()

    def get_photos_list(self, instance):
        photos = Image.objects.filter(animal=instance.id).order_by('priority')
        return ImageGetSerializer(photos, many=True, context=self.context).data


class AnimalSerializer(serializers.ModelSerializer):
    photos = serializers.SerializerMethodField('get_photos_list')
    class Meta:
        model = Animal
        extra_kwargs = {"photos": {"required": False, "allow_null": True}}
        fields =(
            "id",
            "species",
            "name",
            "description",
            "age",
            "health",
            "gender",
            "city",
            "country",
            "status",
            "org",
            "photos",
            "transfer"
        )

    def get_photos_list(self, instance):
        photos = Image.objects.filter(animal=instance.id).order_by('priority')
        return ImageGetSerializer(photos, many=True, context=self.context).data

    def create(self, validated_data):
        photos_data = self.context.get('request')
        animal = Animal.objects.create(**validated_data)
        for photo_data in photos_data:
            id = photo_data["id"]
            priority = photo_data["priority"]
            Image.objects.filter(id=id).update(animal=animal, priority=priority)
        return animal

    def update(self, instance, validated_data):
        photos_data = self.context.get('request')
        Animal.objects.filter(id=instance.id).update(**validated_data)
        for photo_data in photos_data:
            id = photo_data["id"]
            priority = photo_data["priority"]
            Image.objects.filter(id=id).update(animal=instance, priority=priority)
        return instance

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'

class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'

class RequestsGetSerializer(serializers.ModelSerializer):
    animal = serializers.SerializerMethodField('get_animal_list')
    org = serializers.SerializerMethodField('get_org_list')
    user = serializers.SerializerMethodField('get_user_list')
    class Meta:
        model = Request
        fields = (
            'animal',
            'user',
            'org',
            'created',
            'message',
            'email',
            'name'
        )

    def get_animal_list(self, obj):
        return ({
            "id": obj.animal.id,
            "name": obj.animal.name,
        })
    
    def get_org_list(self, obj):
        return obj.org.id

    def get_user_list(self, obj):
        return obj.user.id