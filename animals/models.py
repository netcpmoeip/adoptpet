from django.db import models
from django.apps import apps
from ckeditor.fields import RichTextField
from datetime import datetime

class Country(models.Model):
    country = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = 'Countries'
        verbose_name = 'Country'

    def __str__(self):
        return self.country

class City(models.Model):
    city = models.CharField(max_length=200)
    country = models.ForeignKey(Country, verbose_name='Country', on_delete=models.PROTECT, db_constraint=False)

    class Meta:
        verbose_name_plural = 'Cities'
        verbose_name = 'City'

    def __str__(self):
        return self.city

class Animal(models.Model):
    STATUS = [
        ('active', 'Active'),
        ('process', 'In process'),
        ('adopted', 'Adopted')
    ]
    SPECIES = [
        ('dog', 'Dogs'),
        ('cat', 'Cats'),
        ('other', 'Other animals')
    ]
    AGE = [
        ('kid', 'Young'),
        ('adult', 'Adult'),
        ('senior', 'Senior')
    ]
    HEALTH = [
        ('healthy', 'Healthy'),
        ('special', 'Special needs')
    ]
    GENDER = [
        ('male', 'Male'),
        ('female', 'Female')
    ]
    species = models.CharField(max_length=10, choices=SPECIES, default='cat')
    gender = models.CharField(max_length=6, choices=GENDER, default='male')
    name = models.CharField(max_length=200)
    description = RichTextField(blank=True)
    age = models.CharField(max_length=10, choices=AGE, default='adult')
    health = models.CharField(max_length=10, choices=HEALTH, default='healthy')
    city = models.ForeignKey(City, verbose_name='City', on_delete=models.PROTECT)
    country = models.ForeignKey(Country, verbose_name='Country', on_delete=models.PROTECT, db_constraint=False)
    status = models.CharField(max_length=10, choices=STATUS, default='active')
    pub_date = models.DateTimeField(auto_now_add=True)
    org = models.ForeignKey('users.Profile', null=True, blank=True, on_delete=models.PROTECT)
    transfer = models.BooleanField('Ready to travel', default=False)

    def __str__(self):
        return self.name

class Image(models.Model):
    animal = models.ForeignKey(
        Animal, on_delete=models.CASCADE, related_name="photos", db_constraint=False, null=True, blank=True
    )
    photo = models.ImageField(upload_to="images/")
    priority = models.IntegerField('Priority', null=True)

class Request(models.Model):
    animal = models.ForeignKey(Animal, verbose_name='Animal', on_delete=models.CASCADE, db_constraint=False)
    org = models.ForeignKey('users.Profile', null=True, blank=True, on_delete=models.CASCADE, related_name="org")
    user = models.ForeignKey('users.Profile', null=True, blank=True, on_delete=models.CASCADE, related_name="user")
    email = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    message = RichTextField(blank=True)
    created = models.DateField(default=datetime.now)

