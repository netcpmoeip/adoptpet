# Generated by Django 3.1.7 on 2021-05-13 11:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('animals', '0009_auto_20210505_1349'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='photo',
            field=models.ImageField(upload_to='images/'),
        ),
    ]
