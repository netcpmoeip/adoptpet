from django.contrib import admin
from django.db.models import Q
from animals.models import Animal, City, Country, Image
from django.contrib.admin import AdminSite
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from .forms import AnimalAdminForm
from django.template.loader import get_template

class ImageInline(admin.TabularInline):
    model = Image
    fields = ("priority", "photo",)

class AnimalAdmin(admin.ModelAdmin):
    form = AnimalAdminForm
    inlines = [ImageInline, ]
    save_as = True
    
    def save_related(self, request, form, formsets, change):
        super().save_related(request, form, formsets, change)
        form.save_photos(form.instance)

admin.site.register(Animal, AnimalAdmin)
admin.site.register(City)
admin.site.register(Country)