from django.db import models
from ckeditor.fields import RichTextField

class Page(models.Model):
    title = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)
    content = RichTextField(blank=True)

    class Meta:
        verbose_name_plural = 'Pages'
        verbose_name = 'Page'

    def __str__(self):
        return self.title
