from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import *
from core.serializers import *
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import stripe
import json

@api_view(['GET'])
def page(request, slug):
    if request.method == 'GET':
        data = Page.objects.get(slug=slug)
        serializer = PageSerializer(data)
        return Response(serializer.data)

@csrf_exempt
def create_checkout_session(request):
    if request.method == 'POST':
        domain_url = 'https://adoptpets.eu/'
        stripe.api_key = settings.STRIPE_SECRET_KEY
        data = json.loads(request.body)
        try:
            if data['mode'] == 'payment':
                checkout_session = stripe.checkout.Session.create(
                    success_url=domain_url + 'thanks?session_id={CHECKOUT_SESSION_ID}',
                    cancel_url=domain_url + 'payment-cancelled/',
                    payment_method_types=['card'],
                    mode='payment',
                    line_items=[{
                        'name': 'Donation',
                        'currency': 'eur',
                        'amount': int(data['amount']) * 100,
                        'quantity': 1,
                    }],
                )
            else:
                checkout_session = stripe.checkout.Session.create(
                    success_url=domain_url + 'thanks?session_id={CHECKOUT_SESSION_ID}',
                    cancel_url=domain_url + 'payment-cancelled/',
                    payment_method_types=['card'],
                    mode='subscription',
                    line_items=[{
                        'price': data['price'],
                        'quantity': 1,
                    }],
                )
            return JsonResponse({'sessionId': checkout_session['id']})
        except Exception as e:
            print(str(e))
            return JsonResponse({'error': str(e)})