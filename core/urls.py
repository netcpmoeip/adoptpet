from django.urls import path
from . import views

urlpatterns = [
    path('create-checkout-session/', views.create_checkout_session),
    path('<slug:slug>/', views.page),
]