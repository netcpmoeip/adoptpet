from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from .models import Profile
from animals.models import Animal
from .serializers import *
from animals.serializers import *
from django.shortcuts import get_object_or_404


@api_view(['PUT', 'DELETE', 'GET'])
def user_detail(request):
    try:
        profile = Profile.objects.get(pk=request.user.id)
    except Profile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        user = UserSerializer(profile)
        return Response(user.data)

    elif request.method == 'PUT':
        data = request.data
        serializer = UserSerializer(profile, data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        profile.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def add_to_fav(request):
    if request.method == 'POST':
        id = request.data
        animal = get_object_or_404(Animal, id=id)
        request.user.fav.add(animal)
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def remove_from_fav(request):
    if request.method == 'POST':
        id = request.data
        animal = get_object_or_404(Animal, id=id)
        request.user.fav.remove(animal)
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def user_fav(request):
    try:
        profile = Profile.objects.get(pk=request.user.id)
    except Profile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        favs = profile.fav.all()
        serializer = AnimalGetSerializer(favs, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def user_fav_id(request):
    try:
        profile = Profile.objects.get(pk=request.user.id)
    except Profile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        favs = profile.fav.all()
        ids = profile.fav.values_list('id', flat=True)
        return Response(ids)
