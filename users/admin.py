from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from .forms import ProfileChangeForm, ProfileCreationForm
from .models import Profile

class ProfileAdmin(UserAdmin):    
    add_form = ProfileCreationForm
    form = ProfileChangeForm
    model = Profile
    list_display = ['email']
    ordering = ('email',)
    exclude = ['username']
    fieldsets = (
        ('Personal info', {'fields': ('first_name', 'last_name', 'email', 'password', 'type', 'country')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
    )
    
admin.site.register(Profile, ProfileAdmin)