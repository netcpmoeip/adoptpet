from django.contrib.auth.models import AbstractUser
from django.db import models
from .managers import ProfileManager
from django.apps import apps

class Profile(AbstractUser):
    username = None
    email = models.EmailField(unique=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = ProfileManager()
    TYPE = [
        ('user', 'User'),
        ('org', 'Organization')
    ]
    type = models.CharField(max_length=10, choices=TYPE, default='user')
    country = models.ForeignKey('animals.Country', verbose_name='Country', on_delete=models.PROTECT, blank=True, null=True)
    fav = models.ManyToManyField('animals.Animal', related_name='fav', blank=True, null=True)

    def __str__(self):
        return self.email
