from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm 
from .models import Profile 
from allauth.account.adapter import get_adapter
from allauth.account import app_settings
from allauth.account.app_settings import AuthenticationMethod
from django.core.mail import send_mail
from allauth.account.utils import filter_users_by_email
from allauth.account.forms import ResetPasswordForm
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes


class ProfileCreationForm(UserCreationForm):    
    class Meta:        
        model = Profile        
        fields = ('email', )  

class ProfileChangeForm(UserChangeForm):    
    class Meta:        
        model = Profile        
        fields = UserChangeForm.Meta.fields
        #fields = '__all__'

class CustomPasswordResetForm(ResetPasswordForm):

    email = forms.EmailField(
        label="E-mail",
        required=True,
        widget=forms.TextInput(
            attrs={
                "type": "email",
                "placeholder": "E-mail address",
                "autocomplete": "email",
            }
        ),
    )

    def clean_email(self):
        email = self.cleaned_data["email"]
        email = get_adapter().clean_email(email)
        self.users = filter_users_by_email(email, is_active=True)
        if not self.users:
            raise forms.ValidationError(
                "The e-mail address is not assigned" " to any user account"
            )
        return self.cleaned_data["email"]

    def save(self, request, **kwargs):
        current_site = get_current_site(request)
        email = self.cleaned_data["email"]

        for user in self.users:
            temp_key = default_token_generator.make_token(user)
            # save it to the password reset model
            # password_reset = PasswordReset(user=user, temp_key=temp_key)
            # password_reset.save()

            # send the password reset email
            
            url = 'https://adoptpet.eu/reset/confirm/{uid}/{token}'.format(uid=urlsafe_base64_encode(force_bytes(user.pk)), token=temp_key)

            context = {
                "current_site": current_site,
                "user": user,
                "password_reset_url": url,
                "request": request,
            }

            if app_settings.AUTHENTICATION_METHOD != AuthenticationMethod.EMAIL:
                context["username"] = user_username(user)
            get_adapter(request).send_mail(
                "account/email/password_reset_key", email, context
            )
        return self.cleaned_data["email"]