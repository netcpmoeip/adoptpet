from django.urls import include, path
from allauth.account.views import confirm_email
from django.conf.urls import url
from . import views
from rest_auth.views import PasswordResetConfirmView

urlpatterns = [
    path('auth/', include('rest_auth.urls')),    
    path('auth/register/', include('rest_auth.registration.urls')),
    url(r'^auth/register/account-confirm-email/(?P<key>.+)/$', confirm_email, name='account_confirm_email'),
    url(r'^account/', include('allauth.urls')),
    path('auth/password/reset/confirm/<uidb64>/<token>/', PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('user-detail/', views.user_detail),
    path('add-to-fav/', views.add_to_fav),
    path('remove-from-fav/', views.remove_from_fav),
    path('user-fav/', views.user_fav),
    path('user-fav-id/', views.user_fav_id),
]