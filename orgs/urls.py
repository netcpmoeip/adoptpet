from django.urls import path
from . import views

urlpatterns = [
    path('org/', views.org_detail),
    path('org/<int:id>/', views.about_org),
]