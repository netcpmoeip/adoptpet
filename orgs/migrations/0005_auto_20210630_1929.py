# Generated by Django 3.1.7 on 2021-06-30 19:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orgs', '0004_auto_20210614_0831'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organization',
            name='fb',
            field=models.URLField(blank=True),
        ),
        migrations.AlterField(
            model_name='organization',
            name='site',
            field=models.URLField(blank=True),
        ),
    ]
