from django.db import models
from users.models import Profile
from animals.models import City, Country
from ckeditor.fields import RichTextField


class Organization(models.Model):
    org = models.OneToOneField(Profile, on_delete=models.CASCADE, related_name='organization')
    name = models.CharField(max_length=200, blank=True)
    city = models.ForeignKey(City, verbose_name='City', on_delete=models.PROTECT, blank=True, null=True)
    country = models.ForeignKey(Country, verbose_name='Country', on_delete=models.PROTECT, blank=True, null=True)
    description = RichTextField(blank=True)
    image = models.ImageField(upload_to="orgs/", blank=True, null=True)
    fb = models.URLField(max_length=200, blank=True)
    site = models.URLField(max_length=200, blank=True)

    class Meta:
        verbose_name_plural = 'Organizations'
        verbose_name = 'Organization'

    def __str__(self):
        return str(self.org)