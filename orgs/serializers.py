from rest_framework import serializers
from .models import Organization
from easy_thumbnails.templatetags.thumbnail import thumbnail_url

class OrgSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = '__all__'

class ThumbnailSerializer(serializers.ImageField):
    def __init__(self, alias, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.read_only = True
        self.alias = alias

    def to_representation(self, value):
        if not value:
            return None

        url = thumbnail_url(value, self.alias)
        request = self.context.get('request', None)
        if request is not None:
            return request.build_absolute_uri(url)
        return url

class OrgGetSerializer(serializers.ModelSerializer):
    image = ThumbnailSerializer(alias='small')
    country = serializers.SerializerMethodField('get_country_name')
    city = serializers.SerializerMethodField('get_city_name')
    class Meta:
        model = Organization
        fields ='__all__'

    def get_country_name(self, obj):
        return obj.country.country

    def get_city_name(self, obj):
        return obj.city.city
