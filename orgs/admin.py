from django.contrib import admin
from orgs.models import Organization

# Register your models here.
admin.site.register(Organization)