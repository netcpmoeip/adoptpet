from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from .models import Organization
from .serializers import *
from animals.serializers import *
from animals.models import Animal
from rest_framework.decorators import parser_classes
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
import json


@api_view(['PUT', 'DELETE', 'GET'])
@parser_classes([MultiPartParser, FileUploadParser, FormParser])
def org_detail(request):
    try:
        org = Organization.objects.get(org=request.user.id)
    except Organization.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        data = OrgSerializer(org)
        return Response(data.data)

    elif request.method == 'PUT':
        data = request.data
        serializer = OrgSerializer(org, data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        profile.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def about_org(request, id):
    try:
        org = Organization.objects.get(org=id)
    except Organization.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        animals = Animal.objects.exclude(id=request.query_params['id']).filter(status='active').filter(org=org.org).order_by('?')[:3]
        animal_data = AnimalGetSerializer(animals, many=True)
        if org.country and org.city:
            org_data = OrgGetSerializer(org)
            return Response({
                "animals": animal_data.data,
                "org": org_data.data,
            })
        else:
            return Response({
                "animals": animal_data.data,
                "org": ''
            })
    
